package com.example.widget;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class Checkbox extends AppCompatActivity {
    CheckBox chk1,chk2,chk3;
    Button btn1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkbox);

        chk1 = (CheckBox) findViewById(R.id.checkBox1);
        chk2 = (CheckBox) findViewById(R.id.checkBox2);
        chk3 = (CheckBox) findViewById(R.id.checkBox3);
        btn1 = (Button) findViewById(R.id.button1);

        chk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) {
                    Toast.makeText(getApplicationContext(),"CheckBox-1 Selected",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        chk1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) {
                    Toast.makeText(getApplicationContext(),"CheckBox-1 Selected",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer result = new StringBuffer();
                result.append("รายการที่เลือก : \n");
                if (chk1.isChecked()) {
                    result.append(" CheckBox-1 \n");
                }
                if (chk2.isChecked()) {
                    result.append(" CheckBox-2 \n");
                }
                if (chk3.isChecked()) {
                    result.append(" CheckBox-3 \n");
                }

                Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
            }
        });
    }

}